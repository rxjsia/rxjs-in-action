# RxJS in Action

### Getting Started

```bash
npm install #Installs the necessary dependencies
```

```bash
npm start #Starts up the server
```

Navigate to:

`http://localhost:3000/`

#### Using the samples

Samples are located at

`http://localhost:3000/samples`